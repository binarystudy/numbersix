import { intlDate } from './date';

export const setDividerDate = (date: string) => {
  if (typeof date !== 'string' || !date.length) {
    return '';
  }
  const dateCopy = new Date(date);
  const todayCopy = new Date();
  const today = new Date(todayCopy.getFullYear(), todayCopy.getMonth(), todayCopy.getDate());
  const yesterday = new Date(
    todayCopy.getFullYear(),
    todayCopy.getMonth(),
    todayCopy.getDate() - 1
  );
  if (yesterday > dateCopy) {
    return intlDate.dateDivider(date);
  }
  if (dateCopy >= today) {
    return 'Today';
  }
  return 'Yesterday';
};
