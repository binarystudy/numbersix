import { Message } from "../types";

interface IFetchCustome {
	(url: string): Promise<Message[]>;
}

export const fetchCustome: IFetchCustome = async (url) => {
	const response = await fetch(url);
	if (!response.ok) {
		 return [];
	}
	return await response.json();
};
