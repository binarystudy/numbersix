const create = () => {
  let id = 0;
  return () => {
    id++;
    return `id${id}`;
  };
};

export const createId = create();
