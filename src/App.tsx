import React from 'react';
import { Chat } from './components';
import { URL } from './config';

import './styles/App.css';

function App() {
  return <Chat url={URL} />;
}

export default App;
