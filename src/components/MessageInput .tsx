import React, { useState, useEffect } from 'react';

import '../styles/messageInput.css';

interface Props {
  onSend: (text: string) => void;
  text?: string;
}

export const MessageInput: React.FC<Props> = (props) => {
  const { text = '', onSend } = props;
  const [disabled, setDisablde] = useState<boolean>(true);
  const [value, setValue] = useState<string>(text);
  useEffect(() => {
    setValue(text);
    setDisablde(true);
  }, [text]);
  const handleClick = () => {
    if (value === text || !value.length) {
      return;
    }
    onSend(value);
    setValue('');
    setDisablde(true);
  };
  const handleChange: React.ChangeEventHandler<HTMLTextAreaElement> = (event) => {
    const value = event.target.value;
    setValue(value);
    setDisablde(value === text || !value.length);
  };
  return (
    <div className="message-input">
      <textarea value={value} name="message" onChange={handleChange} className="message-input-text">
        {value}
      </textarea>
      <input
        type="button"
        value="send"
        disabled={disabled}
        onClick={handleClick}
        className={`btn message-input-button${!disabled ? ' btn-success' : ''}`}
      />
    </div>
  );
};
