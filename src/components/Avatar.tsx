import React from 'react';

interface Props {
  src: string;
  alt: string;
}

export const Avatar: React.FC<Props> = (props) => {
  return <img className="message-user-avatar" src={props.src} alt={props.src} />;
};
