import React, { useState, useEffect, useCallback } from 'react';
import { fetchCustome } from '../helpers/fetch';
import { Header } from './Header';
import { MessageList } from './MessageList ';
import { MessageInput } from './MessageInput ';
import { Loader } from './Loader';
import { Message, User } from '../types';
import { getUsersCount, createId } from '../helpers';

import '../styles/chat.css';
import '../styles/buttons.css';

interface Props {
  url: string;
}

const user: User = { user: 'Test', userId: createId() };

export const Chat: React.FC<Props> = ({ url }) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [message, setMessage] = useState<Message | null>(null);
  const [messages, setMessages] = useState<Message[]>([]);
  useEffect(() => {
    const wrapperFetch = async () => {
      let mounted = true;
      const messages = await fetchCustome(url);
      if (!mounted) {
        return;
      }
      setMessages(messages);
      setLoading(false);
      return () => {
        mounted = false;
      };
    };
    wrapperFetch();
  }, [url]);

  const handleDelete = useCallback(
    (id: string) => {
      setMessages((state) => state.filter((message) => message.id !== id));
      if (id === message?.id) {
        setMessage(null);
      }
    },
    [message]
  );

  const handleEdit = useCallback(
    (id: string) => {
      const index = messages.findIndex((message) => message.id === id);
      if (index !== -1) {
        setMessage(messages[index]);
      }
    },
    [messages]
  );

  const handleLiked = (id: string) => {
    setMessages((state) =>
      state.map((message) => {
        if (message.id !== id) {
          return message;
        }
        return {
          ...message,
          liked: !message?.liked,
        };
      })
    );
  };

  const handleSend = (text: string) => {
    setMessages((state) => {
      if (!user.user.length) {
        return state;
      }
      if (!message) {
        return [
          ...state,
          {
            id: createId(),
            ...user,
            avatar: '',
            text,
            createdAt: new Date().toISOString(),
            editedAt: '',
          },
        ];
      }
      return state.map((oldMessage) => {
        if (oldMessage.id !== message.id) {
          return oldMessage;
        }
        return {
          ...oldMessage,
          text,
          editedAt: new Date().toISOString(),
        };
      });
    });
    setMessage(null);
  };

  return loading ? (
    <Loader />
  ) : (
    <div className="chat">
      <Header
        chatName="MyChat"
        messagesCount={messages.length}
        usersCount={getUsersCount(messages)}
        lastMessageDate={messages.length ? messages[messages.length - 1].createdAt : ''}
      />
      <MessageList
        messages={messages}
        onDelete={handleDelete}
        onEdit={handleEdit}
        onLiked={handleLiked}
        user={user}
      />
      <MessageInput onSend={handleSend} text={message?.text} />
    </div>
  );
};
