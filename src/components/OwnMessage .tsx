import React from 'react';
import { intlDate } from '../helpers';
import { Message } from '../types';

import '../styles/message.css';

interface Props {
  message: Message;
  onEdit: () => void;
  onDelete: () => void;
}

export const OwnMessage: React.FC<Props> = (props) => {
  const { message, onEdit, onDelete } = props;
  const { dateMessage, fullDate } = intlDate;
  return (
    <div className="own-message">
      <div className="message-title">
        <span className="message-time">{dateMessage(message.createdAt)}</span>
        {message.editedAt.length ? (
          <span className="message-edit-time">edit: {fullDate(message.editedAt)}</span>
        ) : null}
      </div>
      <span className="message-text">{message.text}</span>
      <button className="message-delete btn btn-light" onClick={() => onDelete()}>
        delete
      </button>
      <button className="message-edit btn btn-success" onClick={() => onEdit()}>
        edit
      </button>
    </div>
  );
};
