import React from 'react';

import '../styles/divider.css';

interface Props {
  text: string;
}

export const Divider: React.FC<Props> = ({ text }) => {
  return (
    <div className="divider">
      <div className="messages-divider">{text} </div>
    </div>
  );
};
